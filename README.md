# Pdf-Office-Eureka-Server


Prior knowledge of following are needed to run the application .
1 . kubernetes 
2 . GCP or aws 
3 . continous integration 

This Container is to be deployed with two other container :-
https://bitbucket.org/shanayasingh_26898/backend-pdf-server/src/master/
https://bitbucket.org/shanayasingh_26898/backend-zuul-proxy/src/master/
This application is made with java spring . It is used for discovery of a service by another service .

The application is Deployed in Kubernetes , the deployment and service file for kubernetes can be found in k8s folder .
The image url has be be configured in deployment file with cloud build or any other build proccess.
the image url in kubernetes file need to be updated accordingly with the build process.

Cloud Build of Google cloud platform is used for Continous integration .
